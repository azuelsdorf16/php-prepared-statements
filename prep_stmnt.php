<?php

#Andrew Zuelsdorf
#17 April 2015
#Proof-of-concept for SQL prepared statements.

function connect_and_safe_query($query, $parameters) {
    #number of question marks and number of parameters
    #must be the same.
    if (substr_count($query, "?") != count($parameters)) {
        return NULL;
    }

    require("connection.php");

    #try to create connection to database host.
    try {

        #Makes a non-persistent connection to the database.
        #Non-persistence was desired because making this
        #connection persistent depends an awful lot on
        #what drivers a given system has.
        $dbh = new PDO(
        "mysql:host=".$HOST.';dbname='.$USERS_DATABASE_NAME,
        $USERS_DATABASE_USERNAME, $USERS_DATABASE_PASSWD);

        #Create a prepared statement
        $statement = $dbh->prepare($query);

        #Fill in the placeholders (the question marks)
        #and return the result of the query
        if ($statement->execute($parameters)) {
            return $statement;
        }
        else {
            return NULL;
        }
    }
    catch (PDOException $e) {
        return NULL;
    }
    catch (Exception $e2) {
        return NULL;
    }

    #no need to close connection. PHP automatically closes
    #connection when script ends. For more info, visit
    #https://php.net/manual/en/pdo.connections.php
}

?>
