<?php

#Andrew Zuelsdorf
#17 April 2015
#Proof-of-concept for SQL prepared statements.

include_once("connection.php");

$search_strings = array("%assassin%", "%assassin%", "%assassin%");

$query = "SELECT * FROM $GAMES_TABLE_NAME WHERE title LIKE ? OR platform LIKE ? OR tags LIKE ?";
#$query = "SELECT * FROM $GAMES_TABLE_NAME WHERE title=?";

echo str_replace("?", $search_strings[0], $query) . "\n";

#try to create connection to database host.
try {

    $dbh = new PDO("mysql:host=".$HOST.';dbname='.$USERS_DATABASE_NAME, $USERS_DATABASE_USERNAME, $USERS_DATABASE_PASSWD, array(PDO::ATTR_PERSISTENT => true));

    #Create a prepared statement
    $statement = $dbh->prepare($query);

    #Fill in the placeholders (the question marks).

    #Execute the statement.
    if ($statement->execute($search_strings)) {
        echo "Execution succeeded!\n";
        while (1) {
            if ($row = $statement->fetch()) {
                echo $row["title"] . "\n";
            }
            else {
                echo "Aaaagh!\n";
                break;
            }
        }
    }
    else {
        echo "Execution failed!\n";
        echo $dbh->errorInfo() . "\n";
    }
}
catch (PDOException $e) {
    echo $e->getMessage();
}
 
?>
