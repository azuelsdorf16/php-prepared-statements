<?php

#Andrew Zuelsdorf
#18 April 2015
#Unit tests for connect_and_safe_query() in prep_stmnt.php

require("prep_stmnt.php");

function tests_for_connect_and_safe_query() {

    require("connection.php");

    $query1 = "SELECT * FROM $GAMES_TABLE_NAME WHERE title LIKE ? OR platform LIKE ? OR tags LIKE ? ?";

    $query2 = "SELECT * FROM $GAMES_TABLE_NAME WHERE title LIKE ? OR platform LIKE ? OR tags LIKE ?";
   
    #Any wildcards used in the original SQL query have to
    #be included in the parameters
    $search_strings2 = array("%assassin%", "%assassin%", "%assassin%");
    
    $result = connect_and_safe_query($query1, $search_strings2);
    
    #The result *should* be null since number of question
    #marks in query (4) and number of parameters (3)
    #were at variance.
    if ($result == NULL) {
        echo "Successfully returned null\n\n";
    }

    $result = connect_and_safe_query($query2, $search_strings2);

    if ($result) {
        echo "Successfully queried database! (No errors in SQL syntax)\n";
        $row = $result->fetch();

        if (!$row) {
            echo "No data returned! Sadness level: 9001\n";
        }
        else {
            echo "Printing titles assocated with query1:\n";
            echo "----------------------------------------\n";

            while ($row) {
                echo $row["title"] . "\n";

                $row = $result->fetch(); 
            }

            echo "If all that lies between the hyphens ";
            echo "above is\n\"Assassin's Creed IV\", then ";
            echo "this test passed.\n";
        }
    }
    else {
        echo "Unsuccessfully queried database! (Errors in SQL syntax)\n";
    }
}

#Run the tests
tests_for_connect_and_safe_query();
 
?>
