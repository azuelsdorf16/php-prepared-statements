<?php
    //This .php file is supposed to act as a "constants" section
    //for connecting to the database and accessing its tables.
    //The information given below is
    //1) IP address of server (so, localhost in this case)
    //2) The name of the database that you want to connect to (my_new_database)
    //3) One variable per table in the database that you want to connect to.
    //So if you had n tables, then you would have n variables for this.
    //I only have one table my database, so I only have $USERS_TABLE_NAME
    //4) The username that you use to access phpMyAdmin (so, root)
    //5) The password that you use to access phpMyAdmin (so, a blank password)
    //I need to fix this, but that can wait.
    $HOST = "104.236.25.55";
    $USERS_DATABASE_NAME = "team5";
    $USERS_TABLE_NAME = "USERS";
	$GAMES_TABLE_NAME = "GAMES";
    $USERS_DATABASE_USERNAME = "team5";
    $USERS_DATABASE_PASSWD = "team5";
?>
